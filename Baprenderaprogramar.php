
<form method="POST">
    <div>
        Introduce 
        <label for="ida"> AX2 </label><input name="a" id="ida" required>
        <label for="idb"> +BX </label><input name="b" id="idb" required>
        <label for="idc"> +C </label><input name="c" id="idc" required>
        <button type="submit" name="pulsar"  > Resolver la ecuación</button>
    </div>
</form>

<?php
if (isset($_POST['pulsar'])){
    extract($_POST, EXTR_SKIP);
    $d=0;
    $e=0;
    $d=$b^2-4*$a*$c;
    $e=2*$a;
    if($d==0){
        echo 'X1=X2='.-$b/$e;
    }else if($d>0){
        echo 'X1='.(-$b+sqrt($d))/$e.'</br>';
        echo 'X2='.(-$b-sqrt($d))/$e.'</br>';
    }else{
        echo 'X1='.-$b/$e.'+'.sqrt(-$d)/$e.'i</br>';
        echo 'X2='.-$b/$e.'-'.sqrt(-$d)/$e.'i</br>';
    }
}
?>
