<!DOCTYPE html>
<!--
FUNCIONES PERSONALIZADAS:
formulario(cantidad_de_cosas,'archivo_donde_lo_envia')   ejemplo: echo formulario(3,'recoge.php')

-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>funciones personalizadas</title>
    </head>
    <body>
        <?php
        // ------------------- FORMULARIO ------------------------------------------------------------------------------------
        function formulario($n_pide,$archivo){
            $d="";
            $d='<form action="'.$archivo.'">';
            for($c=0;$c<$n_pide;$c++):
                $d.= '<div><input name="dato'.($c+1).'"  required placeholder="dato '.($c+1).'"></div>';
            endfor;
            $d.= '<input value="enviar" type="submit">';
            $d.= '<input value="limpiar" type="reset">';
            $d.= '</form>';
            return $d;
        }
        // ------------------- FIN DE FORMULARIO ------------------------------------------------------------------------------
        ?>
        
    </body>
</html>
